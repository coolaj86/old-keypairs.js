#!/usr/bin/env node
'use strict';

var cmd = "npm install --global keypairs-cli";
console.error(cmd);
require('child_process').exec(cmd, function (err) {
  if (err) {
    console.error(err);
    return;
  }
  console.info("Run 'keypairs help' to see what you can do!");
});
